﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventCode
    {
        WhoFinishedEventCode = 0
    }

    private int finishOrder = 0;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void onDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte) RaiseEventCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nickNameOffFinishedPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            GameObject orderUiText = RacingGameManager.instance.finisherTextUi[finishOrder - 1];
            orderUiText.SetActive(true);

            if (viewId == photonView.ViewID)
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOffFinishedPlayer + "(YOU)";
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else 
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOffFinishedPlayer;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject go in RacingGameManager.instance.lapTriggers)
        {
            lapTriggers.Add(go);    
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (lapTriggers.Contains(collider.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(collider.gameObject);

            lapTriggers[indexOfTrigger].SetActive(false);
        }

        if (collider.gameObject.CompareTag("Finish Trigger"))
        {
            GameFinish();
        }
    }
    
    public void GameFinish()
    {
        GetComponent<PlayerSetup>().gameCamera.transform.parent = null;
        GetComponent<VehicleMovementScript>().enabled = false;

        finishOrder++;
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        // event data
        object[] data = new object[] { nickName, finishOrder, viewId };
        
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions {
            Reliability = false 
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoFinishedEventCode, data, raiseEventOptions, sendOption);
    }
}
