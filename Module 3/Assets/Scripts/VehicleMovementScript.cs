﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovementScript : MonoBehaviour
{
   public float speed = 40;
   public float rotSpeed = 200;
   public float currentSpeed = 0;
   
   public bool isControlEnabled;

   void Start()
   {
       isControlEnabled = false;
   }
   void LateUpdate()
   {
       if (isControlEnabled)
       {
        float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal")  * rotSpeed * Time.deltaTime;

        transform.Translate(0, 0, translation);
        currentSpeed = translation;

        transform.Rotate(0, rotation, 0);
       }
       
   }
}
