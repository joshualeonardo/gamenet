﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviourPunCallbacks
{
    public GameObject owner;
    public int damage = 10;
      
    private Rigidbody rbBullet;
    private int bulletSpeed = 40;
    private int lifeSpan = 3;

    void Start()
    {
      rbBullet = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rbBullet.AddForce(rbBullet.transform.forward * bulletSpeed);
        Destroy(gameObject, lifeSpan);
    }
    public void OnTriggerEnter(Collider collider)
    {
      if (collider.gameObject == owner) return;
      if (collider.gameObject.CompareTag("Player") && !collider.GetComponent<PhotonView>().IsMine)
      {
          collider.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
          PhotonNetwork.Destroy(this.gameObject);
      }
    }
}
