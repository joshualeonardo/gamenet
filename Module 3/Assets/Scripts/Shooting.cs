﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Shooting : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    public Camera camera; 
    public GameObject turret;
    public GameObject bullet;
    public int playerCount;

    [Header ("HP Related")]
    public int currentHealth;
    private int maxHealth = 100;
    public Image healthbar;

    void Start()
    {
        healthbar.fillAmount = currentHealth/maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        playerCount = DeathRaceManager.instance.playerCount;
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3 (0.5f, 0.5f));

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, 200))
            {
                Debug.Log(hit.collider.gameObject.name);
            }

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);
            }
        }

        else if (Input.GetMouseButtonDown(1))
        {
            PhotonNetwork.Instantiate("bullet", turret.transform.position, turret.transform.rotation);
            bullet.GetComponent<Projectile>().owner = this.gameObject;
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.currentHealth -= damage;
        this.healthbar.fillAmount = currentHealth / maxHealth;

        if (currentHealth <= 0)
        {
            PlayerDeath();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    public enum RaiseEventsCode
    {
        WhoLostEventCode = 0,
        WhoWonEventCode = 1
    }

    void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoWonEventCode)
        {
            object[] data =(object[])photonEvent.CustomData;

            string nickNameOffFinishedPlayer = (string)data[0];
            int viewId = (int)data[1];

            DeathRaceManager.instance.finisherTextUi[1].SetActive(true);
            DeathRaceManager.instance.finisherTextUi[0].SetActive(false);

            Text text = DeathRaceManager.instance.finisherTextUi[1].GetComponent<Text>();
            text.text = "You are the winner " + nickNameOffFinishedPlayer + "!";
        }

        else if (photonEvent.Code == (byte)RaiseEventsCode.WhoLostEventCode)
        {
            playerCount--;
            object[] data =(object[])photonEvent.CustomData;

            string nickNameOffFinishedPlayer = (string)data[0];
            int viewId = (int)data[1];
            playerCount = (int)data[2];

            DeathRaceManager.instance.finisherTextUi[1].SetActive(false);
            DeathRaceManager.instance.finisherTextUi[0].SetActive(true);
            

            Text text = DeathRaceManager.instance.finisherTextUi[1].GetComponent<Text>();
            text.text = "You lost " + nickNameOffFinishedPlayer;

            if (playerCount <= 1)
            {
                PlayerWon();
            }
        }
    }

    public void PlayerDeath()
    {
        GetComponent<PlayerSetup>().gameCamera.transform.parent = null;
        GetComponent<Shooting>().enabled = false;
        GetComponent<VehicleMovementScript>().enabled = false;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId, playerCount };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoLostEventCode, data, raiseEventOptions, sendOptions);
    }

    public void PlayerWon()
    {
        GetComponent<PlayerSetup>().gameCamera.transform.parent = null;
        GetComponent<VehicleMovementScript>().enabled = false;
        GetComponent<Shooting>().enabled = false;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoWonEventCode, data, raiseEventOptions, sendOptions);
    }
}
