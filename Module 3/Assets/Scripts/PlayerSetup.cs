﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera gameCamera;
    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        this.gameCamera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            gameCamera.enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = photonView.IsMine;            
            gameCamera.enabled = photonView.IsMine;
        }
        text.text = photonView.Owner.NickName;
    }
}
