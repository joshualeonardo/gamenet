﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Shooting : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    public Camera camera; 
    public GameObject turret;
    public GameObject bullet;
    public int playerCount;
    public bool canShoot;

    [Header ("HP Related")]
    public float currentHealth;
    private float maxHealth = 100;
    public Image healthbar;

    
    void Start()
    {
        currentHealth = maxHealth;
        healthbar.fillAmount = currentHealth/maxHealth;
        canShoot = false;
    }

    // Update is called once per frame
    void Update()
    {
        playerCount = RacingGameManager.instance.playerCount;

        if (Input.GetMouseButtonDown(0) && canShoot)
        {
            PhotonNetwork.Instantiate("bullet", turret.transform.position, turret.transform.rotation);
            bullet.GetComponent<Projectile>().owner = this.gameObject;
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.currentHealth -= damage;
        this.healthbar.fillAmount = currentHealth / maxHealth;

        if (currentHealth <= 0)
        {
            PlayerDeath();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    public void PlayerDeath()
    {
        GetComponent<VehicleMovementScript>().enabled = false;
        GetComponent<Shooting>().enabled = false;

        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(3);
        GetComponent<VehicleMovementScript>().enabled = true;
        GetComponent<Shooting>().enabled = true;
    }

}