﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;
    public GameObject lastOneToHit;

    [Header("HP Related")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    [Header("Kill Related")]
    public int killCounter;
    RaycastHit hit;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Fire()
    {
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("createHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
                lastOneToHit = hit.collider.gameObject.GetComponent<PhotonView>().GetComponent<GameObject>();
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
            GameObject killFeedText = GameObject.Find("KillfeedText");
            killFeedText.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
            StartCoroutine(KillFeed(killFeedText));
            lastOneToHit.GetComponent<Shooting>().killCounter += 1;
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }
    
    /*[PunRPC]
    public void AddKillCount(int value)
    {
        this.killCounter += value;
    }*/

    [PunRPC]
    public void createHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText"); 
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = " ";



        this.transform.position = SpawnPoints.instance.waypoints[Random.Range(0, 4)].transform.position;
        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    IEnumerator KillFeed(GameObject killfeed)
    {
        yield return new WaitForSeconds(3.0f);
        killfeed.GetComponent<Text>().text = " ";
    }

    [PunRPC]
    public void RegainHealth()
    {
        transform.GetComponent<PlayerMovementController>().enabled = true;
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
