﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthBar;
    public float startingHealth =  100;
    public float currentHealth; 

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = startingHealth;
        healthBar.fillAmount = currentHealth/startingHealth;
    }

    [PunRPC]
    public void Damage(int damage)
    {
        currentHealth -= damage;
        healthBar.fillAmount = currentHealth/startingHealth;
        if(currentHealth < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if(photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
    }
}
